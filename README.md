# DataWarden
DataWarden is a secure data storage with CLI interface.

## Installation
With a correctly configured Go toolchain:
```bash
go get -u gitlab.com/stukanya/datawarden
```

## Structure
* AES and SHA256 algorhitms for secure encryption user data.
* MVC architecture style service with interface injection.
* PostgreSQL as user data (pin code and files) database.
* Cobra as CLI interface
