package store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/stukanya/datawarden/model/auth"
)

var (
	_ auth.SessionRepository = (*SessionStorage)(nil)
)

type SessionStorage struct {
	db *sql.DB
}

func NewSessionStorage(db *sql.DB) *SessionStorage {
	return &SessionStorage{
		db: db,
	}
}

func (s *SessionStorage) InitTable(ctx context.Context) error {
	const query = `CREATE TABLE IF NOT EXISTS warden.session (
			id			uuid 	PRIMARY KEY DEFAULT uuid_generate_v4(),
			user_id 	uuid	NOT NULL REFERENCES warden.user(id),
			expires_at 	time	NOT NULL DEFAULT(now() at time zone 'utc')
		);`
	_, err := s.db.ExecContext(ctx, query)
	if err != nil {
		return fmt.Errorf("failed to create session table: %v", err)
	}

	return nil
}

func (s *SessionStorage) StoreSession(ctx context.Context, session *auth.Session) (uuid.UUID, error) {
	session.ID = uuid.NewV4()
	const query = "INSERT INTO Session (id, user_id, expires_at) VALUES ($1, $2, $3)"
	_, err := s.db.ExecContext(ctx, query, session.ID, session.User, session.ExpiresAt)
	if err != nil {
		return uuid.Nil, fmt.Errorf("failed to register new Session: %v", err)
	}

	return session.ID, nil
}

func (s *SessionStorage) FetchSession(ctx context.Context, session *auth.Session) error {
	const query = "SELECT user_id, expires_at FROM session WHERE id=?"
	err := s.db.QueryRowContext(ctx, query, session.ID).Scan(session.User, session.ExpiresAt)
	switch {
	case errors.Is(err, sql.ErrNoRows):
		return auth.SessionNotFoundErr
	case err != nil:
		return fmt.Errorf("unable to select Session from db: %v", err)
	}
	return nil
}
