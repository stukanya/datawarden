package store

import (
	"context"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/stukanya/datawarden/model/auth"
)

var (
	_ auth.SessionRepository = (*SessionStoreMock)(nil)
)

type SessionStoreMock struct {
	sessionData map[uuid.UUID]*auth.Session
}

func NewSessionStoreMock() *SessionStoreMock {
	return &SessionStoreMock{sessionData: make(map[uuid.UUID]*auth.Session)}
}

func (s *SessionStoreMock) StoreSession(ctx context.Context, session *auth.Session) (uuid.UUID, error) {
	sessionID := uuid.NewV4()
	s.sessionData[sessionID] = session
	return sessionID, nil
}

//nolint:staticcheck
func (s *SessionStoreMock) FetchSession(ctx context.Context, session *auth.Session) error {
	var ok bool
	session, ok = s.sessionData[session.ID]
	if !ok {
		return auth.SessionNotFoundErr
	}
	return nil
}
