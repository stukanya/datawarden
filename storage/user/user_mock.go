package store

import (
	"context"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/stukanya/datawarden/model/user"
)

var (
	_ user.UserRepository = (*UserStoreMock)(nil)
)

type UserStoreMock struct {
	userData map[uuid.UUID]*user.User
}

func NewStoreMock() *UserStoreMock {
	return &UserStoreMock{userData: make(map[uuid.UUID]*user.User)}
}

//nolint:staticcheck
func (s *UserStoreMock) FetchUser(ctx context.Context, usr *user.User) error {
	var ok bool
	usr, ok = s.userData[usr.ID]
	if !ok {
		return user.UserNotFoundErr
	}
	return nil
}

func (s *UserStoreMock) StoreUser(ctx context.Context, usr *user.User) (uuid.UUID, error) {
	userID := uuid.NewV4()
	s.userData[userID] = usr
	return userID, nil
}
