package store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/stukanya/datawarden/model/user"
)

var (
	_ user.UserRepository = (*UserStorage)(nil)
)

type UserStorage struct {
	db *sql.DB
}

func NewUserStorage(db *sql.DB) *UserStorage {
	return &UserStorage{
		db: db,
	}
}

func (s *UserStorage) InitTable(ctx context.Context) error {
	const query = `CREATE TABLE IF NOT EXISTS warden.user (
			id			uuid 	PRIMARY KEY DEFAULT uuid_generate_v4(),
			name 		text	NOT NULL UNIQUE,
			created_at 	time	NOT NULL DEFAULT(now() at time zone 'utc'),
			is_disabled	boolean NOT NULL DEFAULT false
		);`
	_, err := s.db.ExecContext(ctx, query)
	if err != nil {
		return fmt.Errorf("failed to create user table: %v", err)
	}

	return nil
}

func (s *UserStorage) StoreUser(ctx context.Context, usr *user.User) (uuid.UUID, error) {
	usr.ID = uuid.NewV4()
	const query = "INSERT INTO user (id, name, created_at, is_active) VALUES ($1, $2, $3, $4)"
	_, err := s.db.ExecContext(ctx, query, usr.ID, usr.Name, usr.RegisteredAt, usr.IsDisabled)
	if err != nil {
		return uuid.Nil, fmt.Errorf("failed to register new user: %v", err)
	}

	return usr.ID, nil
}

func (s *UserStorage) FetchUser(ctx context.Context, usr *user.User) error {
	const query = "SELECT name, created_at, is_disabled FROM user WHERE id=?"
	err := s.db.QueryRowContext(ctx, query, usr.ID).Scan(usr.Name, usr.RegisteredAt, usr.IsDisabled)
	switch {
	case errors.Is(err, sql.ErrNoRows):
		return user.UserNotFoundErr
	case err != nil:
		return fmt.Errorf("unable to select user from db: %v", err)
	}
	return nil
}
