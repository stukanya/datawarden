package store

import (
	"context"
	"database/sql"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/stukanya/datawarden/model/auth"
	"gitlab.com/stukanya/datawarden/model/crypter"
)

var (
	_ auth.PinCodeStorer     = (*PinCodeStorage)(nil)
	_ crypter.PinCodeFetcher = (*PinCodeStorage)(nil)
)

type PinCodeStorage struct {
	db *sql.DB
}

func NewPinCodeStorage(db *sql.DB) *PinCodeStorage {
	return &PinCodeStorage{db: db}
}

func (s *PinCodeStorage) InitTable(ctx context.Context) error {
	const query = `CREATE TABLE IF NOT EXISTS warden.pin_code (
			id			uuid 	PRIMARY KEY DEFAULT uuid_generate_v4(),
			user_id  	uuid 	NOT NULL REFERENCES warden.user(id),
			hash 		bytea 	NOT NULL
		);`
	_, err := s.db.ExecContext(ctx, query)
	if err != nil {
		return fmt.Errorf("failed to create pin code table: %v", err)
	}

	return nil
}

func (s *PinCodeStorage) StorePinHash(ctx context.Context, userID uuid.UUID, pinHash []byte) error {
	const query = "INSERT INTO warden.pin_code (user_id, hash) VALUES ($1, $2)"
	_, err := s.db.ExecContext(ctx, query, userID, pinHash)
	if err != nil {
		return fmt.Errorf("failed to store new pin code: %v", err)
	}

	return nil
}

func (s *PinCodeStorage) FetchPinHash(ctx context.Context, userID uuid.UUID) ([]byte, error) {
	const query = "SELECT hash FROM warden.pin_code WHERE user_id=?"
	row := s.db.QueryRowContext(ctx, query, userID)
	if err := row.Err(); err != nil {
		return nil, fmt.Errorf("unable to select pin code hash from db: %v", err)
	}

	var pinHash []byte
	if err := row.Scan(&pinHash); err != nil {
		return pinHash, fmt.Errorf("unable to scan code hash: %v", err)
	}

	return pinHash, nil
}
