package store

import (
	"context"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	uuid "github.com/satori/go.uuid"
)

// Test mock-DB
func TestInsertDB(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("unable to init sql mock: %v", err)
	}
	defer db.Close()

	ctx := context.Background()
	st := NewPinCodeStorage(db)

	info := PinMock{
		id:   uuid.NewV4(),
		data: []byte{'0', 'x', 'd', 'e', 'a', 'd'},
	}

	mock.ExpectExec("INSERT INTO warden.pin_code").
		WithArgs(info.id, info.data).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = st.StorePinHash(ctx, info.id, info.data)
	if err != nil {
		t.Errorf("unable to store test pin hash data: %v", err)
	}

	mock.ExpectQuery("SELECT hash FROM warden.pin_code WHERE user_id=\\?").
		WithArgs(info.id).
		WillReturnRows(sqlmock.NewRows([]string{"hash"}).AddRow(info.data))

	_, err = st.FetchPinHash(ctx, info.id)
	if err != nil {
		t.Errorf("unable to fetch test pin hash data: %s", err)
	}

	err = mock.ExpectationsWereMet()
	if err != nil {
		t.Errorf("There were unfulfilled expectations: %s", err)
	}
}
