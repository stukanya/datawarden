# syntax=docker/dockerfile:1

FROM golang:1.17-buster

RUN apt-get update && \
    apt-get install --no-install-suggests --no-install-recommends --yes \
   make

RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.42.1

WORKDIR /data-warden

COPY go.mod .
COPY go.sum .
RUN go mod download

ADD . /data-warden

RUN make build