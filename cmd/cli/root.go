package cli

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/stukanya/datawarden/model/user"
)

var (
	rootCmd = &cobra.Command{
		Use:   "dwarden",
		Short: "DataWarden is a secure data storage with CLI and REST API interfaces.",
	}
	userRegistry *user.UserRegistry
)

func Execute(ctx context.Context, users *user.UserRegistry) error {
	if _, err := rootCmd.ExecuteContextC(ctx); err != nil {
		return fmt.Errorf("unable to execute CLI command: %v", err)
	}
	userRegistry = users
	return nil
}
