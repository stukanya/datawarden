package cli

import (
	"fmt"

	"github.com/spf13/cobra"
)

var version = "DataWarden v0.1 -- HEAD"

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of DataWarden.",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(version)
	},
}
