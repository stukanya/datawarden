package cli

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(userCmd)
	userCmd.AddCommand(userCreateCmd)
}

var userCmd = &cobra.Command{
	Use:   "user",
	Short: "Command uses to administrate user info.",
}

var userCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "Create new user with given username.",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return errors.New("invalid number of user args")
		}
		userName := args[0]
		if len(userName) < 1 && len(userName) > 10 {
			return errors.New("invalid name of user name")
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		userID, err := userRegistry.RegisterUser(cmd.Context(), args[0])
		if err != nil {
			return fmt.Errorf("unable to register new user: %v", err)
		}
		fmt.Printf("New user registered: %s\n", userID)
		return nil
	},
}
