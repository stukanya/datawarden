package main

import (
	"context"
	"database/sql"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/stukanya/datawarden/api"
	"gitlab.com/stukanya/datawarden/model/auth"
	"gitlab.com/stukanya/datawarden/model/user"

	cryptStorage "gitlab.com/stukanya/datawarden/storage/crypto"
	pinStorage "gitlab.com/stukanya/datawarden/storage/pin"
	sessionStorage "gitlab.com/stukanya/datawarden/storage/session"
	userStorage "gitlab.com/stukanya/datawarden/storage/user"

	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "./config.yml", "path to config file")
}

func main() {
	// Init context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Init OS sigquit channel
	sigquit := make(chan os.Signal, 1)
	signal.Notify(sigquit, os.Interrupt, syscall.SIGINT)

	go func() {
		oscall := <-sigquit
		log.Printf("syscall:%+v", oscall)
		cancel()
	}()

	// Load config from YAML file
	config := NewConfig()
	err := config.Parse()
	if err != nil {
		log.Fatal(err)
	}

	// Configure logger
	logger := logrus.New()
	level, err := logrus.ParseLevel(config.Logger.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	logger.SetLevel(level)

	// Configure DB and store layer
	dbURL := config.DbConfig.DatabaseURL
	db, err := newDB(dbURL)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// User storage
	userSt := userStorage.NewUserStorage(db)
	err = userSt.InitTable(ctx)
	if err != nil {
		log.Fatal(err)
	}
	userRegistry := user.NewUserRegistry(userSt)

	// Init storages
	cryptSt := cryptStorage.NewCryptoStorage(db)
	err = cryptSt.InitTable(ctx)
	if err != nil {
		log.Fatal(err)
	}

	pinSt := pinStorage.NewPinCodeStorage(db)
	err = pinSt.InitTable(ctx)
	if err != nil {
		log.Fatal(err)
	}

	sessionSt := sessionStorage.NewSessionStorage(db)
	err = sessionSt.InitTable(ctx)
	if err != nil {
		log.Fatal(err)
	}
	authRegistry := auth.NewSessionRegistry(logger, sessionSt, userSt, pinSt)

	// CLI startup
	//go func() {
	//	if err := cli.Execute(ctx, userRegistry); err != nil {
	//		log.Fatal(err)
	//	}
	//}()

	// Server startup
	server := api.NewAPIServer(logger, &config.ServerConfig, userRegistry, authRegistry)
	err = server.Serve(ctx)
	if err != nil {
		log.Fatal(err)
	}
}

// Open database
func newDB(dbURL string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dbURL)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return db, err
	}

	return db, nil
}
