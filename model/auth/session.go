package auth

import (
	"context"
	"crypto/sha256"
	"errors"
	"fmt"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"

	"gitlab.com/stukanya/datawarden/model/user"
)

var (
	SessionNotFoundErr = errors.New("session not found")
)

type Session struct {
	ID        uuid.UUID
	User      uuid.UUID
	ExpiresAt time.Time
}

type SessionRegistry struct {
	log         *logrus.Logger
	sessionRepo SessionRepository
	userRepo    user.UserRepository
	pinRepo     PinCodeStorer
}

func NewSessionRegistry(
	log *logrus.Logger,
	session SessionRepository,
	user user.UserRepository,
	pin PinCodeStorer,
) *SessionRegistry {
	return &SessionRegistry{
		log:         log,
		sessionRepo: session,
		userRepo:    user,
		pinRepo:     pin,
	}
}

func (s *SessionRegistry) RegisterSession(ctx context.Context, userID uuid.UUID) (uuid.UUID, error) {
	usr := &user.User{ID: userID}
	err := s.userRepo.FetchUser(ctx, usr)
	if err != nil {
		return uuid.Nil, fmt.Errorf("unable to getch user info: %v", err)
	}
	if usr.IsDisabled {
		return uuid.Nil, errors.New("user is disabled")
	}

	session := &Session{
		User:      usr.ID,
		ExpiresAt: time.Now().Add(time.Hour),
	}
	sessionID, err := s.sessionRepo.StoreSession(ctx, session)
	if err != nil {
		return uuid.Nil, fmt.Errorf("unable to store session: %v", err)
	}

	return sessionID, nil
}

func (s *SessionRegistry) CheckSessionExpiration(ctx context.Context, sessionID uuid.UUID) (bool, error) {
	now := time.Now()
	session := &Session{
		ID: sessionID,
	}
	err := s.sessionRepo.FetchSession(ctx, session)
	if err != nil {
		return false, fmt.Errorf("unable to fetch session: %v", err)
	}
	return session.ExpiresAt.After(now), nil
}

func (s *SessionRegistry) RegisterPinCode(ctx context.Context, userID uuid.UUID, pin string) error {
	shaHash := sha256.New()
	shaHash.Write([]byte(pin))

	if err := s.pinRepo.StorePinHash(ctx, userID, shaHash.Sum(nil)); err != nil {
		return fmt.Errorf("unable to store pin code: %v", err)
	}
	return nil
}

func (s *SessionRegistry) Authenticate(ctx context.Context, userID uuid.UUID, pin string) (bool, error) {
	// TODO: check user and pass hash
	return true, nil
}

type SessionRepository interface {
	SessionStorer
	SessionFetcher
}

type SessionStorer interface {
	StoreSession(ctx context.Context, session *Session) (uuid.UUID, error)
}

type SessionFetcher interface {
	FetchSession(ctx context.Context, session *Session) error
}

type PinCodeStorer interface {
	StorePinHash(ctx context.Context, userID uuid.UUID, pin []byte) error
}
