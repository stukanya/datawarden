package user

import (
	"context"
	"errors"
	"fmt"
	"time"

	uuid "github.com/satori/go.uuid"
)

var (
	UserNotFoundErr = errors.New("user not found")
)

type User struct {
	ID           uuid.UUID
	Name         string
	RegisteredAt time.Time
	IsDisabled   bool
}

type UserRegistry struct {
	userRepo UserRepository
}

func NewUserRegistry(user UserRepository) *UserRegistry {
	return &UserRegistry{
		userRepo: user,
	}
}

func (s *UserRegistry) RegisterUser(ctx context.Context, userName string) (uuid.UUID, error) {
	userInfo := &User{
		Name:         userName,
		RegisteredAt: time.Now(),
		IsDisabled:   false,
	}

	userID, err := s.userRepo.StoreUser(ctx, userInfo)
	if err != nil {
		return uuid.Nil, fmt.Errorf("unable to register new user: %v", err)
	}
	return userID, nil
}

func (s *UserRegistry) FetchUser(ctx context.Context, user *User) error {
	if user.ID == uuid.Nil {
		return errors.New("user ID is not presented")
	}

	err := s.userRepo.FetchUser(ctx, user)
	if err != nil {
		return fmt.Errorf("unable to fetch user: %v", err)
	}
	return nil
}

type UserRepository interface {
	UserStorer
	UserFetcher
}

type UserStorer interface {
	StoreUser(ctx context.Context, user *User) (uuid.UUID, error)
}

type UserFetcher interface {
	FetchUser(ctx context.Context, user *User) error
}
