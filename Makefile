ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
DOCKER_FLAGS ?= --rm=true
DOCKER_BUILD_FLAGS ?= --force-rm=true --pull --rm=true
DOCKER_IMAGE := stukanya/data-warden
DOCKER_TAG ?= latest
VERSIONED_IMAGE:= $(DOCKER_IMAGE):$(DOCKER_TAG)
DOCKER_CONTAINER_NAME := data-warden

build:
	go build -v ${ROOT_DIR}/cmd/...

test:
	go test -race ${ROOT_DIR}/...

lint :
	cd ${ROOT_DIR} && \
		golangci-lint --build-tags testing run --max-same-issues 0 --max-issues-per-linter 0

.PHONY: docker-build
docker-build:
	docker build $(DOCKER_BUILD_FLAGS) --tag "$(VERSIONED_IMAGE)" "$(ROOT_DIR)"

.PHONY: docker-lint
docker-lint:
	docker run $(DOCKER_FLAGS) "$(VERSIONED_IMAGE)" \
		make lint

.PHONY: docker-test
docker-test:
	docker run $(DOCKER_FLAGS) "$(VERSIONED_IMAGE)" \
		make test

.PHONY: docker-clean
docker-clean:
	docker rmi -f "$$(docker images -q $(VERSIONED_IMAGE))"
