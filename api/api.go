package api

import (
	"context"
	"net"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"

	"gitlab.com/stukanya/datawarden/model/auth"
	"gitlab.com/stukanya/datawarden/model/user"
)

type APIServer struct {
	config *ServerConfig
	logger *logrus.Logger
	users  *user.UserRegistry
	auth   *auth.SessionRegistry
}

func NewAPIServer(
	log *logrus.Logger,
	config *ServerConfig,
	users *user.UserRegistry,
	auth *auth.SessionRegistry,
) *APIServer {
	return &APIServer{
		config: config,
		logger: log,
		users:  users,
		auth:   auth,
	}
}

func (s *APIServer) Serve(ctx context.Context) error {
	r := gin.Default()

	r.POST("/auth", basicAuth(s.auth), s.registerAuthSession)

	cookieAuth := r.Group("/", sessionCookie(s.auth))

	cookieAuth.GET("/user", s.fetchCurrentUser)

	cookieAuth.POST("/crypt", s.storeData)
	cookieAuth.GET("/crypt/:id", s.fetchData)

	server := http.Server{
		Addr:    s.config.BindAddr,
		Handler: r,
		BaseContext: func(listener net.Listener) context.Context {
			return ctx
		},
	}

	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			s.logger.Fatal("Error during server listening: ", err)
		}
	}()
	s.logger.Info("Server is listening, URL: ", s.config.BindAddr)

	// Gracefully shutdown HTTP server...
	<-ctx.Done()
	s.logger.Info("Server stopped")
	ctxShutDown, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := server.Shutdown(ctxShutDown); err != nil {
		s.logger.Fatal("Unable to shutdown server: ", err)
	}
	s.logger.Info("Gracefully shutdown server")

	return nil
}

func ServerError(err error) gin.H {
	return gin.H{"error": err}
}

func (s *APIServer) fetchCurrentUser(c *gin.Context) {
	// TOTO: get user ID from session cookie
	userID, err := uuid.FromString(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ServerError(err))
	}
	user := &user.User{
		ID: userID,
	}
	err = s.users.FetchUser(c.Request.Context(), user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, ServerError(err))
	}

	userView := gin.H{
		"id":           userID,
		"name":         user.Name,
		"registeredAt": user.RegisteredAt,
	}
	c.JSON(http.StatusOK, userView)

	s.logger.Info("[API] fetch user info, ID: ", userID)
}

func (s *APIServer) registerAuthSession(c *gin.Context) {
	user := c.MustGet(gin.AuthUserKey).(string)
	sessionID, err := s.auth.RegisterSession(c.Request.Context(), uuid.FromStringOrNil(user))
	if err != nil {
		c.JSON(http.StatusInternalServerError, ServerError(err))
	}

	c.SetCookie("session", sessionID.String(), 60, "/", "localhost", false, true)
}

func (s *APIServer) storeData(c *gin.Context) {

}

func (s *APIServer) fetchData(c *gin.Context) {

}

func sessionCookie(auth *auth.SessionRegistry) gin.HandlerFunc {
	return func(c *gin.Context) {
		if cookie, err := c.Cookie("session"); err == nil {
			expired, authErr := auth.CheckSessionExpiration(c.Request.Context(), uuid.FromStringOrNil(cookie))
			if expired && authErr == nil {
				c.Next()
				return
			}
		}

		c.JSON(http.StatusForbidden, gin.H{"error": "Forbidden with no cookie"})
		c.Abort()
	}
}

func basicAuth(auth *auth.SessionRegistry) gin.HandlerFunc {
	return func(c *gin.Context) {
		user, password, hasAuth := c.Request.BasicAuth()
		if hasAuth {
			auth, err := auth.Authenticate(c.Request.Context(), uuid.FromStringOrNil(user), password)
			if auth && err == nil {
				return
			}
		}

		c.Abort()
		c.Writer.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
	}
}
